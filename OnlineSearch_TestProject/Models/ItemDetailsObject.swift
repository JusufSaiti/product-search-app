//
//  ItemDetailsObject.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf on 10/16/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit
import EVReflection

class ItemDetailsObject: EVObject {

    var id:String = ""
    var site_id:String = ""
    var title:String = ""
    var subtitle:String = ""
    var seller_id:Int = 0
    var category_id:String = ""
    var official_store_id:String = ""
    var price:Int = 0
    var base_price:Int = 0
    var original_price:Int = 0
    var currency_id:String = ""
    var initial_quantity:Int = 0
    var available_quantity:Int = 0
    var sold_quantity:Int = 0
    var sale_terms:[Any] = []
    var buying_mode:String = ""
    var listing_type_id:String = ""
    var start_time:String = ""
    var stop_time:String = ""
    var condition:String = ""
    var permalink:String = ""
    var thumbnail:String = ""
    var secure_thumbnail:String = ""
    var pictures:[PictureObject] = []
    var video_id:String = ""
    var descriptions:[DescriptionObject] = []
    var accepts_mercadopago:Bool = false
    var non_mercado_pago_payment_methods:[NonMercadoPagoPaymentMethodObject] = []
    var international_delivery_mode:String = ""
    var shipping:ShippingObject!
    var seller_address:SellerAddressObject!
    var seller_contact:NSObject!
    var location:NSObject!
    var geolocation:GeoLocationObject!
    var coverage_areas:[Any] = []
    var attributes:[AttributeObject] = []
    var warnings:[Any] = []
    var listing_source:String = ""
    var variations:[Any] = []
    var status:String = ""
    var sub_status:[Any] = []
    var tags:[Any] = []
    var warranty:String = ""
    var catalog_product_id:NSObject!
    var domain_id:NSObject!
    var parent_item_id:NSObject!
    var differential_pricing:NSObject!
    var deal_ids:[Any] = []
    var automatic_relist:Bool = false
    var date_created:String = ""
    var last_updated:String = ""

}

class PictureObject: EVObject {
    
    var id:String = ""
    var url:String = ""
    var secure_url:String = ""
    var size:String = ""
    var max_size:String = ""
    var quality:String = ""
}

class DescriptionObject: EVObject {
    var id:String = ""
}

class GeoLocationObject: EVObject {
    var latitude:String = ""
    var longitude:String = ""
}

class NonMercadoPagoPaymentMethodObject: EVObject {
    var id:String = ""
    var type:String = ""
}
