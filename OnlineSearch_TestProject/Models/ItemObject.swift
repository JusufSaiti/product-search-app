//
//  ItemObject.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit
import EVReflection

class ItemObject: EVObject {

    var id:String = ""
    var site_id:String = ""
    var title:String = ""
    var seller:SellerObject!
    var price:Int = 0
    var currency_id:String = ""
    var reservation_price:NSObject!
    var reservation_currency_id:NSObject!
    var available_quantity:Int = 0
    var sold_quantity:Int = 0
    var buying_mode:String = ""
    var listing_type_id:String = ""
    var stop_time:String = ""
    var condition:String = ""
    var permalink:String = ""
    var thumbnail:String = ""
    var accepts_mercadopago:Bool = false
    var installments:InstallmentsObject!
    var address:AddressObject!
    var shipping:ShippingObject!
    var seller_address:SellerAddressObject!
    var attributes:[AttributeObject]!
    var original_price:NSObject!
    var category_id:String = ""
    var official_store_id:NSObject!
    var catalog_product_id:NSObject!
    var reviews:ReviewsObject!
    
    
}

class SellerObject: EVObject {
    var id:String = ""
    var power_seller_status:String = ""
    var car_dealer:Bool = false
    var real_estate_agency:Bool = false
    var tags:[Any] = []
}

class InstallmentsObject: EVObject {
    var quantity:Int = 0
    var amount:Float = 0.0
    var rate:Int = 0
    var currency_id:String = ""
}

class AddressObject: EVObject {
    var state_id:String = ""
    var state_name:String = ""
    var city_id:String = ""
    var city_name:String = ""
}

class ShippingObject: EVObject {
    var free_shipping:Bool = false
    var mode:String = ""
    var tags:[Any] = []
    var methods:[Any] = []
    var dimensions:NSObject!
    var local_pick_up:Bool = false
    var logistic_type:String = ""
    var store_pick_up:Bool = false
    
}


class SellerAddressObject: EVObject {
    var id:Int = 0
    var comment:String = ""
    var address_line:String = ""
    var zip_code:String = ""
    var country:CountryObject!
    var state:StateObject!
    var city:CityObject!
    var search_location:SearchLocationObject!
    var latitude:String = ""
    var longitude:String = ""
}


class CountryObject: EVObject {
    var id:String = ""
    var name:String = ""
}


class StateObject: EVObject {
    var id:String = ""
    var name:String = ""
}


class CityObject: EVObject {
    var id:String = ""
    var name:String = ""
}

class NeighborhoodObject: EVObject {
    var id:String = ""
    var name:String = ""
}

class ReviewsObject: EVObject {
    var rating_average:Float = 0.0
    var total:Int = 0
}


class AttributeObject: EVObject {
    var attribute_group_id:String = ""
    var attribute_group_name:String = ""
    var id:String = ""
    var name:String = ""
    var value_id:String = ""
    var value_name:String = ""
    var value_struct:String = ""
}

class SearchLocationObject: EVObject {
    var neighborhood:NeighborhoodObject!
    var city:CityObject!
    var state:StateObject!
}


