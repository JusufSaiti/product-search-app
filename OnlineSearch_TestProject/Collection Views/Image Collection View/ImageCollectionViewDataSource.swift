//
//  ImageCollectionViewDataSource.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf on 10/16/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit

class ImageCollectionViewDataSource: NSObject {

    var arrayPhotos:[UIImage] = []
    
    init(photos:[UIImage], collectionView:UICollectionView) {
        collectionView.register(ImageCollectionViewCell.nib, forCellWithReuseIdentifier: ImageCollectionViewCell.key)
        self.arrayPhotos = photos
        super.init()
    }
    
}

extension ImageCollectionViewDataSource: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.key, for: indexPath) as! ImageCollectionViewCell

        cell.imageView.image = arrayPhotos[indexPath.row]
        return cell
    }
}
