//
//  ImageCollectionViewCell.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf on 10/16/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    public static let nib = UINib.init(nibName: "ImageCollectionViewCell", bundle: .main)
    public static let key: String = "ImageGalleryCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
