//
//  APICalls.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit
import Alamofire

class APICalls {
    
    static  func loadItemsAPICall(type:String, completeHandler: @escaping (_ accountObject: [ItemObject]?, _ error:Bool)  -> Void ) {
        
        let params:Parameters = ["q": type]
        
        NetworkRequest.makeAPICall(url: APIConstants.getAllItemsAPI, method: .get, parameters: params) { (response, error) in
            
            if error == nil{
                        
                       let dict:[NSDictionary] = (response as! NSDictionary).object(forKey: "results") as! [NSDictionary]
                        let results = [ItemObject](dictionaryArray: dict)
                        completeHandler(results, false)
                        
                    }else{
                        completeHandler(nil, true)
                    }
          
        }
    }
    
    static  func itemDetailsAPI(itemID:String, completeHandler: @escaping (_ accountObject: ItemDetailsObject?, _ error:Bool)  -> Void ) {
        
        let params:Parameters = [:]
        let uri:String = "\(APIConstants.getItemDetailsAPI)/\(itemID)"
        
        NetworkRequest.makeAPICall(url: uri, method: .get, parameters: params) { (response, error) in
            
            if error == nil{
                
                let dict:NSDictionary = (response as! NSDictionary)
                let detailsObject = ItemDetailsObject(dictionary: dict)
                completeHandler(detailsObject, false)
                
            }else{
                completeHandler(nil, true)
            }
            
        }
    }


}
