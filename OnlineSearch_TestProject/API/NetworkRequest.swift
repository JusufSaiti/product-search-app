//
//  NetworkRequest.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit
import Alamofire

class NetworkRequest {
    
    static func makeAPICall(url: String, method: HTTPMethod, parameters:[String: Any], completeHandler: @escaping (_ response: Any?,_ error: Error?)-> Void)  {
        
        Alamofire.request(url, method: method, parameters: parameters).responseJSON {
            response in
            
            if response.response?.statusCode == 200{
                
                if let result = response.result.value{
                    
                    completeHandler(result, nil)
                    
                }else{
                    
                    completeHandler(nil, response.error)
                    
                }
            }else{
                
                completeHandler(nil,response.error)
            }
        }
    }
}
