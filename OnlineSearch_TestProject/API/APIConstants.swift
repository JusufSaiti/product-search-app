//
//  APIConstants.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit

class APIConstants {
    
    private static let httpsDomain:String = "https://api.mercadolibre.com"
    
    public static let getAllItemsAPI:String = ("\(httpsDomain)/sites/MLU/search")
    public static let getItemDetailsAPI:String = ("\(httpsDomain)/items")

}
