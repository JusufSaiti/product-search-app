//
//  ViewController.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit
import SVProgressHUD

class ViewController: UIViewController {

    @IBOutlet weak var tableViewItems: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var viewLastViewedItems: UIView!
    @IBOutlet weak var imgViewRecentItem0: UIImageView!
    @IBOutlet weak var imgViewRecentItem1: UIImageView!
    @IBOutlet weak var imgViewRecentItem2: UIImageView!
    @IBOutlet weak var imgViewRecentItem3: UIImageView!
    @IBOutlet weak var imgViewRecentItem4: UIImageView!
    @IBOutlet weak var bottomConstraintForRecentItemsView: NSLayoutConstraint!
    
  
    var recentItems:[ItemObject] = []
    var tblViewDataSource: MainTableViewDataSource!
    var tblViewDelegate:MainTableViewDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appearenceObjects()
        addGestureRecognizers()
        recentItems = AppDelegate.stateController.getrecentItemsFromPermanentStorage()

        tblViewDelegate = MainTableViewDelegate()
        tblViewDataSource = MainTableViewDataSource(tableView: tableViewItems)
        
        tableViewItems.delegate = tblViewDelegate
        tableViewItems.dataSource = tblViewDataSource
        searchBar.delegate = tblViewDataSource
        tblViewDataSource.delegate = self
        
        loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showRecentItems()
    }
    
    func appearenceObjects() -> Void {
        self.navigationItem.title = NSLocalizedString("online search", comment: "")
        bottomConstraintForRecentItemsView.constant = 100
    }

    func loadData() -> Void {
        
        SVProgressHUD.show(withStatus: NSLocalizedString("loading data", comment: ""))
        APICalls.loadItemsAPICall(type: "notebook") { (items, error) in
            SVProgressHUD.dismiss()
            if !error {
                self.tblViewDataSource.tableItems = items!
                self.tableViewItems.reloadData()
                
            }else{
                
                Helper.showAlert(title: "error", message: "error durring loading data" , buttons: ["cancel", "try again"], completion: { (id) in
                    
                    if id == 1{
                        self.loadData()
                    }
                })
            }
        }
    }
    
    func addItemToRecentLict(item:ItemObject) -> Void {
        
        if recentItems.count == 0{
            recentItems.append(item)
        }else{
            
            for i in recentItems{
                if i.id  == item.id{
                    return
                }
            }
            
            if recentItems.count == 5 {
                recentItems.remove(at: 0)
            }
            
            recentItems.append(item)
        }
        
        AppDelegate.stateController.saveToPermanentStorageRecentItems(items: recentItems)
    }
    
    
    func showRecentItems() -> Void {
        if recentItems.count != 0{
            bottomConstraintForRecentItemsView.constant = 0
            var i = 0
            var url:String = ""
            
            while i < recentItems.count {
                
                url = recentItems[i].thumbnail

                if let imgView = self.viewLastViewedItems.viewWithTag(i + 1) as? UIImageView {
                    Helper.getImageFromURL(url: url, completation: { (data, error) in
                        if !error{
                            if let image:UIImage = UIImage.init(data: data!){
                                imgView.image = image
                            }
                        }
                    })
                }
                i += 1
            }
        }
    }
    
    func addGestureRecognizers() -> Void {
        let didTap0 = UITapGestureRecognizer(target: self, action: #selector(didTapRecentItem(sender:)))
        let didTap1 = UITapGestureRecognizer(target: self, action: #selector(didTapRecentItem(sender:)))
        let didTap2 = UITapGestureRecognizer(target: self, action: #selector(didTapRecentItem(sender:)))
        let didTap3 = UITapGestureRecognizer(target: self, action: #selector(didTapRecentItem(sender:)))
        let didTap4 = UITapGestureRecognizer(target: self, action: #selector(didTapRecentItem(sender:)))
        
        imgViewRecentItem0.addGestureRecognizer(didTap0)
        imgViewRecentItem1.addGestureRecognizer(didTap1)
        imgViewRecentItem2.addGestureRecognizer(didTap2)
        imgViewRecentItem3.addGestureRecognizer(didTap3)
        imgViewRecentItem4.addGestureRecognizer(didTap4)

    }
    
    func didTapRecentItem(sender:UITapGestureRecognizer) -> Void {

        self.performSegue(withIdentifier: "showDetailSegueFromRecentList", sender: sender)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "showDetailSegueFromTableView" ,
            let nextScene = segue.destination as? ItemDetailViewController ,
            let indexPath = self.tableViewItems.indexPathForSelectedRow {
            var item:ItemObject!
            if tblViewDataSource.searchActive{
                item = tblViewDataSource.searchResults[indexPath.row]
            }else{
                item = tblViewDataSource.tableItems[indexPath.row]
            }
            tableViewItems.deselectRow(at: indexPath, animated: true)
            nextScene.item = item
            addItemToRecentLict(item: item)
            print("\(recentItems)\n***************************************************")
        }else if segue.identifier == "showDetailSegueFromRecentList" ,
            let nextScene = segue.destination as? ItemDetailViewController,
            let index:Int = (sender as! UITapGestureRecognizer).view?.tag{
            let item:ItemObject = recentItems[index - 1]
            nextScene.item = item
        }
    }
}


extension ViewController: RefreshMainTableViewDelegate{
   
    func reloadSearchData() {
        tableViewItems.reloadData()
    }
}
