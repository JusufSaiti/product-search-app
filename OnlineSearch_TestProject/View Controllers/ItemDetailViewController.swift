//
//  ItemDetailViewController.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/16/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit
import SVProgressHUD

class ItemDetailViewController: UIViewController {

    var item:ItemObject!
    var itemDetails:ItemDetailsObject!
    var imageCollectionViewDataSource:ImageCollectionViewDataSource!
    var imageCollectionViewDelegate:ImageCollectionViewDelegate!
    var images:[UIImage] = []
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCondition: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblAvaliableQuantity: UILabel!
    @IBOutlet weak var lblSoldQuantity: UILabel!
    @IBOutlet weak var lblWarranty: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageCollectionViewDataSource = ImageCollectionViewDataSource(photos: images, collectionView: imagesCollectionView)
        imagesCollectionView.dataSource = imageCollectionViewDataSource
        
        loadData()

    }

    func loadData() -> Void {
        SVProgressHUD.show()
        
        if item != nil{
            APICalls.itemDetailsAPI(itemID: item.id, completeHandler: { (itemDetailsObject, error) in
                if !error{
                    self.itemDetails = itemDetailsObject
                    self.showData()
                    self.loadPhotos(completion: { (photos) in
                        self.imageCollectionViewDataSource.arrayPhotos = photos
                        self.imagesCollectionView.reloadData()
                        SVProgressHUD.dismiss()
                    })
                }
            })
        }
    }
    
    
    func loadPhotos(completion: @escaping ([UIImage]) -> Void) {
        
        var photos:[UIImage] = []
        var j = 0
        
        for i in itemDetails.pictures {
            
            Helper.getImageFromURL(url: i.url, completation: { (data, error) in
                
                if let img:UIImage = UIImage.init(data: data!){
                    photos.append(img)
                }
                j += 1
                
                if j == self.itemDetails.pictures.count{
                    completion(photos)
                }
            })
        }
    }
    
    
    func showData() -> Void {
        
        lblTitle.text = itemDetails.title
        lblStatus.text = itemDetails.status
        lblCondition.text = itemDetails.condition
        lblPrice.text = "\(itemDetails.price) \(itemDetails.currency_id)"
        lblAvaliableQuantity.text = "\(itemDetails.available_quantity) of \(itemDetails.initial_quantity)"
        lblSoldQuantity.text = "\(itemDetails.sold_quantity)"
        lblWarranty.text = itemDetails.warranty
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
