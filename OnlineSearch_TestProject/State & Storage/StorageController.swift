//
//  StorageController.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf on 10/17/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit

class StorageController {

    
    func saveToPermanentStorageRecentItems(items:[ItemObject]) -> Void {
        
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: items), forKey: "recentItems")
        UserDefaults.standard.synchronize()
    }
    
}
