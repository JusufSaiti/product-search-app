//
//  StateController.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf on 10/17/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit

class StateController {
    
    fileprivate let storageController: StorageController

    init(storageController: StorageController) {
        self.storageController = storageController
        
    }

    func saveToPermanentStorageRecentItems(items:[ItemObject]) -> Void {
       storageController.saveToPermanentStorageRecentItems(items: items)
    }
    
    func getrecentItemsFromPermanentStorage() -> [ItemObject] {
        if let data:Data = UserDefaults.standard.value(forKey: "recentItems") as? Data{
            let items:[ItemObject] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [ItemObject]
            return items
        }
        return []
    }
   
}
