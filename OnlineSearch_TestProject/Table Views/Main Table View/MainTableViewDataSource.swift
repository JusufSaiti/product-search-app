//
//  MainTableViewDataSource.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit

protocol RefreshMainTableViewDelegate: class {
    func reloadSearchData()
}

class MainTableViewDataSource: NSObject {
    
    var tableItems: [ItemObject] = []
    var searchResults:[ItemObject] = []
    var searchActive:Bool = false
    weak var delegate: RefreshMainTableViewDelegate?
    
    
    init(tableView: UITableView) {
        
        tableView.register(MainTableViewCell.nib, forCellReuseIdentifier: MainTableViewCell.key)
        super.init()
    }
}


extension MainTableViewDataSource: UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var numOfSections: Int = 1
        
        if tableItems.count != 0{
            
            tableView.separatorStyle = .singleLine
            numOfSections            =  1
            tableView.backgroundView = nil
            
        } else {
            
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = NSLocalizedString("no data", comment: "")
            noDataLabel.textColor     = UIColor.darkGray
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        
        return numOfSections
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchActive {
            return searchResults.count
        }
        
        return tableItems.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MainTableViewCell.key, for: indexPath) as! MainTableViewCell
        
        var item:ItemObject!
        
        if searchActive {
            
            item = searchResults[indexPath.row]
            
        }else{
            
            item = tableItems[indexPath.row]
        }
        
        cell.labelTitle.text = item.title
        cell.labelPrice.text = "\(item.price) \(item.currency_id)"
        cell.accessoryType = .disclosureIndicator

        Helper.getImageFromURL(url: item.thumbnail) { (data, error) in
            if !error {
                if let img:UIImage = UIImage.init(data: data!){
                    cell.imgView.image = img
                }
            }
        }
        
        return cell
        
    }
}

// MARK: SearchBar Delegate
extension MainTableViewDataSource: UISearchBarDelegate {
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchResults = self.tableItems.filter({ (result: ItemObject) -> Bool in
            
           return  result.title.lowercased().contains(searchText.lowercased())
            
        })
        
  
        
        if(searchText == ""){
            searchActive = false;
        } else {
            searchActive = true;
        }
        
        delegate?.reloadSearchData()
        
    }
}



