//
//  MainTableViewCell.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    public static let nib = UINib.init(nibName: "MainTableViewCell", bundle: .main)
    public static let key: String = "MainTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
