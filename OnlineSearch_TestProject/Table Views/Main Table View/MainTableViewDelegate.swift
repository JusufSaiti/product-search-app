//
//  MainTableViewDelegate.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit

class MainTableViewDelegate: NSObject {

    override init() {
        super.init()
    }
}

extension MainTableViewDelegate: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        UIApplication.topMostViewController?.view.endEditing(true)
        UIApplication.topMostViewController?.performSegue(withIdentifier: "showDetailSegueFromTableView", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
