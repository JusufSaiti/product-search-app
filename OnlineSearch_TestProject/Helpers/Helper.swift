//
//  Helper.swift
//  OnlineSearch_TestProject
//
//  Created by Jusuf Saiti on 10/15/17.
//  Copyright © 2017 Codex Developers. All rights reserved.
//

import UIKit
import Alamofire

class Helper: NSObject {

    
    static func getImageFromURL(url:String, completation: @escaping (_ data: Data?, _ error: Bool ) -> Void ){
        
        Alamofire.request(url, method: .get).response { (response) in

            if response.response?.statusCode == 200{

                completation(response.data, false)

            }else{

                completation(response.data, true)

            }
        }
    }
    
    
    static func showAlert(title: String, message: String, buttons:[String] = [], completion: ((_ id: Int?) -> Void)? = nil){
        
        var localizedButtons:[String] = []
        
        for i in buttons{
            localizedButtons.append(NSLocalizedString(i, comment: ""))
        }
        
        let alert = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString(message, comment: ""), preferredStyle: .alert)
        if buttons.count == 0 {
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: { (a) in
                
                completion?(localizedButtons.index(of: a.title!))
                
            })
            
            alert.addAction(cancelAction)
            
        } else{
            
            var i:Int = 0
            
            while i < buttons.count {
                
                if i == 0 {
                    
                    let firstAction = UIAlertAction(title: NSLocalizedString("\(buttons[i])", comment: "") , style: .cancel, handler: { (a) in
                        
                        completion?(localizedButtons.index(of: a.title!))
                        
                    })
                    
                    alert.addAction(firstAction)
                    
                }else{
                    
                    let otherAction = UIAlertAction(title: NSLocalizedString("\(buttons[i])", comment: ""), style: .default, handler: { (a) in
                        
                        completion?(localizedButtons.index(of: a.title!))
                        
                    })
                    
                    alert.addAction(otherAction)
                }
                
                i += 1
            }
        }
        
        UIApplication.topMostViewController?.present(alert, animated: true, completion: nil)
    }
}
